/*
 * Made by Dwijen on 11/16/2018
 */

/*
 * Made by Dwijen on 11/16/2018
 */


import java.util.ArrayList;

public class Bird extends animal implements FlyingObject, EggLayer {
    boolean inTheAir = false;
    boolean hasHadAccident = false;
    String name;
    ArrayList<Integer> batches = new ArrayList<Integer>();
    ArrayList<Bird> children = new ArrayList<Bird>();

    public Bird(String name){
        this.name = name;
    }

    public void fly(){
        inTheAir = true;

        double crashPercentage = Math.random();
        if (crashPercentage < 0.5){
            hasHadAccident = true;
            inTheAir = false;
        }
    }

    public boolean isFlying() {
        if (inTheAir){
            return true;
        }else{
            return false;
        }
    }

    public String makeName(){
        int numLetters = (int)(Math.random() * 10) + 1;
        String name = "";
        for (int i = 0; i < numLetters; i++){
            name += (char)((Math.random() * 91) + 32);
        }
        return name;
    }

    public void lay(){
        if (hasHadAccident){
            return;
        }
        int eggsLaid = (int)(Math.random() * 18);
        batches.add(eggsLaid);
    }
    public void hatch(){
        if (hasHadAccident){
            return;
        }
        int amountEggs = batches.remove(0);
        System.out.println("Your bird laid " + amountEggs + " eggs!");
        for (int i = 0; i < amountEggs; i++){
            double hatchPercentage = Math.random();
            if (hatchPercentage < 0.79){
                children.add(new Bird(makeName()));
            }
        }

    }

    public ArrayList<Bird> getChildren(){
        return children;
    }
}
