/*
 * Made by Dwijen on 11/10/2018
 */

public class Dodecagon extends Shape{
    int base1;
    int base2;
    int height;

    public int getArea(){
        return (base1 + base2) * height / 2;
    }
}
