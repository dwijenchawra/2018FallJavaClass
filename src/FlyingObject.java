/*
 * Made by Dwijen on 11/16/2018
 */

/*
 * Made by Dwijen on 11/16/2018
 */

public interface FlyingObject {
    boolean hasWings = true;

    void fly();

    boolean isFlying();
}
