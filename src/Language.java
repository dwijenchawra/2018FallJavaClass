/*
 * Made by Dwijen on 10/7/2018
 */

public class Language extends Languages {
    int skill;
    public Language(){
        super("java", 0);
        skill = 0;
        System.out.println("You are learning " + type + "!");
    }
    public void practice(){
        if (skill == 10){
            System.out.println("Wow you have mastered" + type);
        }
        System.out.println("You practiced " + type + " a little more!");
        skill++;
    }
    public int getSkill(){
        return skill;
    }
    public void takeClass(){
        skill += 4;
    }


}
