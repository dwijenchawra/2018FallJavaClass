/*
 * Made by Dwijen on 10/7/2018
 */

public class Languages {
    String type;
    int programs = 0;
    int skill = 0;
    public Languages(){
        System.out.println("You just learned a language!");
    }
    public Languages(String type1, int skill1){
        type = type1;
        skill = skill1;
    }
    public void takeClass(){
        skill += 2;
    }
    public void makeCode(){
        if (programs == 1){
            System.out.println("Nice, you made your first program!");
        }
        programs++;
    }
    public String getType(){
        return "This language is " + type;
    }
}
