/*
 * Made by Dwijen on 11/10/2018
 */

public abstract class Shape{
    int numSides;

    public int getSides(){
        return numSides;
    }

    public abstract int getArea();
}
