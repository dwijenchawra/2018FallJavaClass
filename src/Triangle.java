/*
 * Made by Dwijen on 11/10/2018
 */

public class Triangle extends Shape {
    int base;
    int height;

    public int getArea(){
        return base * height / 2;
    }
}
