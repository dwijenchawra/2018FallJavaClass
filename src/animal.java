/*
 * Made by Dwijen on 10/6/2018
 */

public class animal {
    int age = 0;
    String type = "unknown";
    public animal(){
        System.out.println("An animal just fell from the sky.");
    }
    public animal(int age1, String type1){
        age = age1;
        type = type1;
    }
    //class methods
    public int getAge(){
        return age;
    }
    public void setAge(int x){
        age = x;
    }

}
