/*
 * Made by Dwijen on 10/6/2018
 */

public class dog extends animal {
    public dog(){
        super(1, "doge");
        System.out.println("This animal is a doge");
    }
    public void aging(){
        age += 20;
    }

    public int getAge(){
        return 30;
    }
    public void bark(){
        System.out.println("Doge has much barked!");
    }
}
