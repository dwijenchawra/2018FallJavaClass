/*
 * Made by Dwijen on 11/16/2018
 */

public class hw2class7 extends hwclass7 implements hwinterface{
    String typeofhw = "";

    @Override
    public void type(String x) {
        typeofhw = x;
    }

    @Override
    public void hwcomplete() {
        System.out.println("You completed your " + typeofhw + " homework!");
    }
}
