public class week1 {
    public static void main(String[] args) {
        //INT AND STRING

        int a = 10;
        int x, y, z;
        x = 3;
        int b = a + x;
        System.out.println(x);
        System.out.print(a);
        //no newline char after print
        System.out.println(x);
        System.out.println(b);

        String s = "12121";
        System.out.println(s);

        int c = Integer.parseInt(s);
        //converts string to int

        //BOOLEANS

        boolean bool = true;
        System.out.println(bool);

        //CONDITIONALS

        if (bool) {
            System.out.println("Trueee");
        }

        if (0 > 1) {
            System.out.println("true");
        }

        for (int i = 0; i <= 100; i++) {
            System.out.println(i);
        }

        //+= x=x+x
        //*= x=x*x
        //  /= x= x/x

        int num = 0;

        while (num < 10) {
            System.out.println(num);
            num++;
        }

        int hundred = 0;

        for (int i = 0; i <= 100; i++) {
            hundred += i;
        }
        System.out.println(hundred);

        int count = 0;
        int total = 0;
        while (count <= 100) {
            total += count;
            count++;
        }
        System.out.println(total);

        int number = 10;
        int divisor = 0;

        if (divisor == 0 || number / divisor == 5) {
            System.out.println(number + " divided by " + divisor + " is 5");
        }

        int c1 = 0;
        while (true) {
            c1+=2;
            System.out.println(c1);
            if (c1 == 20) {
                break;
            }
        }

        //double makes it decimal
        //use double vs float because float cuts it off a bit early

        double a12 = 10/3;
        System.out.println(a12);



        int num3 = Integer.MAX_VALUE;
        while (num3 != 0) {
            num3 /= 2;
            System.out.println(num3);
        }
        System.out.println(".");
        int foo = 360;
        int counter = 1;
        while (counter <= foo) {
            if (360 % counter == 0) {
                System.out.println(counter);
            }
            counter++;
        }

        String xd = "* ";

        for (int i = 0; i < 5; i++) {
            System.out.println(xd);
            xd += "* ";
        }
        //
        //
        //
        //
        //
        //WEEK 1 HW

        double counterr = 0;
        double numberr = 0;
        while (true) {
            if (numberr > 1000000000) {
                break;
            }
            numberr = Math.pow(2, counterr);
            System.out.println(numberr);
            counterr++;
        }


        for (int i = 1; i <= 15; i++) {
            for (int j = 1; j <= 15; j++) {
                if (j == 15) {
                    System.out.println(i * j);
                    System.out.println(" ");
                    System.out.println(" ");
                    break;
                }
                System.out.print(i * j + "	");
            }
        }
        for (int i = 1; i <= 15; i++) {
            for (int j = 1; j <= 15; j++) {
                System.out.printf("%4d", i * j + "	");
            }
            System.out.print("\n");
        }
    }
}
