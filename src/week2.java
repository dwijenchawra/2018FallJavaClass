import java.util.ArrayList;

public class week2 {
    public static void main(String[] args){
        /**
         * ARRAYS
         * they are faster than lists in multiple for loops
         */
        int [] a1 = {4, 6, -1, 14};

        for (int i = 0; i < a1.length; i++){
            System.out.println(a1[i]);
        }

        String[] a2 = {"hello", "abc", "xyz"};

        for (String i:a2) System.out.println(i);
/*
        for (int i = 0; i < a1.length; i++){
            a1[i] = 0;
        }
*/
        //this type of loop only works for arrays and is only used for printing values.
        for (int i:a1){
            i = 0;
        }

        for (int i = 0; i < a1.length; i++) System.out.println(a1[i]);


        int sum = 0;

        for (int i = 0; i < a1.length; i++) {
            sum += a1[i];
            System.out.println("Sum: " + sum);
        }

        int biggest = Integer.MIN_VALUE;
        for (int i = 0; i < a1.length; i++){
            if (a1[i] > biggest){
                biggest = a1[i];
            }
        }
        System.out.println("Biggest: " + biggest);

        //CHAR ARRAYS

        char[] a4 = new char[5];
        a4[3] = 99;

        a4[3] = 't';
        //print empty char because nothing was assigned
        System.out.println(a4[0]);
        System.out.println(a4[3]);

        System.out.println("*");
        System.out.println("*");
        System.out.println("*");
        System.out.println("*");
        System.out.println("*");
        System.out.println("*");

        /**
         * ARRAYLIST
         * no fixed size
         * watch for index shifting when you add or remove an index
        */

        ArrayList<Integer> alist1 = new ArrayList<>();

        alist1.add(5);
        alist1.add(4);
        alist1.add(3);
        alist1.add(2);
        alist1.add(1);

        alist1.add(6);



        System.out.println(alist1.get(1));
        alist1.remove(1);
        alist1.add(alist1.get(1));
        System.out.println(alist1.get(1));
        System.out.println(alist1.size());

        alist1.add(1, 4);

        for (int i = 0; i < alist1.get(i); i++) {
            System.out.println(alist1.get(i));
        }
        for (int i:alist1){
            System.out.println(i);
        }

        ArrayList<Integer> alist2 = new ArrayList<>();

        alist2.add(0);
        alist2.add(1);
        alist2.add(1);
        alist2.add(1);
        alist2.add(2);
        alist2.add(3);
        alist2.add(4);

        for (int i = 0; i < alist2.size(); i++){
            if (alist2.get(i) == 1) {
                alist2.remove(i);
                i--;
            }
        }
        /*

        Alt method with traversing backwards

        for (int i = alist.size(); i >= 0; i--){
            if (alist2.get(i) == 1) {
                alist2.remove(i);
            }
        }
        */

        System.out.println(alist2.toString());


        //HOMEWORK

        int[] hwarray = {4, 22, 17, -6, 17, -25, 22};
        int max = Integer.MIN_VALUE;
        int max2 = Integer.MIN_VALUE;
/*

        for (int i = 0; i < hwarray.length; i++){
            if ((hwarray[i] > max)){
                max = hwarray[i];

            }
        }


        for (int i = 0; i < hwarray.length; i++){
            if ((hwarray[i] > max2) && (hwarray[i] < max)){
                max2 = hwarray[i];
            }

        }
*/


        ArrayList<Integer> even = new ArrayList<>();
        ArrayList<Integer> odd = new ArrayList<>();

        for (int i = 0; i < hwarray.length; i++){
            if (hwarray[i] % 2 == 0){
                even.add(hwarray[i]);
            }
            else{
                odd.add(hwarray[i]);
            }
        }


        for (int i = 0; i < hwarray.length; i++){
            if (hwarray[i] > max){
                max2 = max;
                max = hwarray[i];
            }
            else if (hwarray[i] > max2 && hwarray[i] != max){
                max2 = hwarray[i];
            }
        }


        System.out.println("Second max: " + max2);
        System.out.println("Even: " + even.toString());
        System.out.println("Odd: " + odd.toString());




    }

}



