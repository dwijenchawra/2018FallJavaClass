/*
 * Made by Dwijen on 10/6/2018
 */
import java.util.ArrayList;

public class week3 {
    public static void main(String[] args) {
        //class creation: make a new class file and constructor
        animal a = new animal(40, "doge");
        System.out.println(a.getAge());
        a.setAge(50);
        System.out.println(a.getAge());
        dog d = new dog();
        d.aging();
        d.bark();

        /**
         * polymorhism
         *
         */
        animal a1 = new dog();
        ((dog)a1).bark();

        dog d1 = (dog)a1;


        ArrayList<animal> theJungle = new ArrayList<animal>();
        theJungle.add(a);
        theJungle.add(d);


        for (int i = 0; i < theJungle.size(); i++){
            animal curr = theJungle.get(i);
            if (curr.getClass().equals(d1.getClass())){
                System.out.println("In the jungle...");
                d.bark();
            }
        }
    }
}
