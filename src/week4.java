/*
 * Made by Dwijen on 10/13/2018
 */

import java.util.Arrays;
import java.util.ArrayList;

public class week4 {

    public static int InsertionSort(int[] x){
        int counter = 0;
        for (int i = 1; i < x.length; i++){
            for (int j = i-1; j>= 0; j--){
                if (x[j]<x[j+1]){
                    //swap
                    int temp = x[j+1];
                    x[j+1] = x[j];
                    x[j] = temp;
                    //System.out.println(Arrays.toString(x));
                    counter++;
                }else{
                    break;
                }
            }
        }
        return counter;
    }

    public static void SelectionSort(int[] x){
        for (int i = 0; i < x.length -1; i++){
            int min = i;
            for (int j = i+1; j<x.length; j++){
                if (x[j] < x[min]) {
                    min = j;
                }
            }
            //swap
            int temp = x[i];
            x[i] = x[min];
            x[min] = temp;
            if (i == 4){
                System.out.println(Arrays.toString(x));
                break;
            }
        }

    }
    public static int BinarySearch(int[] x, int y){
        //BINARY SEARCH
        int counter = 0;
        int l = 0, r = x.length - 1;
        while (l <= r){
            int m = (r + l)/2;
            //check if x is present at mid
            if (x[m] == y){
                //System.out.println(m);
                break;
            }
            //if x is greater, ignore left half
            if (y > x[m]){
                l = m + 1;
                counter++;
            }
            //if x is smaller, ignore right side
            else {
                r = m - 1;
                counter++;
            }
        }
        return counter;
    }
    public static int ArrayListSearch(ArrayList<Integer> x, int y){
        //BINARY SEARCH
        int counter = 0;
        int l = 0, r = x.size() - 1;
        while (l <= r){
            int m = (r + l)/2;
            //check if x is present at mid
            if (x.get(m) == y){
                //System.out.println(m);
                break;
            }
            //if x is greater, ignore left half
            if (y > x.get(m)){
                l = m + 1;
                counter++;
            }
            //if x is smaller, ignore right side
            else {
                r = m - 1;
                counter++;
            }
        }
        return counter;
    }
    public static void main(String[] args) {
        int[] arr3 = {10, 7, 6, -3, 4, 5, 1, 2, 18};

        int[] hwarr1 = {5, 1, 2, 3, 4, 7, 6, 9};
        int[] hwarr2 = {9, 5, 1, 4, 3, 2, 1, 0};
        int[] hwarr3 = {9, 4, 6, 2, 1, 5, 1, 3};
        int[] hwarr4 = {9, 6, 9, 5, 6, 7, 2, 0};
        System.out.println("Question 1:");
        System.out.println(InsertionSort(hwarr1));
        System.out.println(InsertionSort(hwarr2));
        System.out.println(InsertionSort(hwarr3));
        System.out.println(InsertionSort(hwarr4));
        int[] hwarr5 = {-4, 1, 2, 6, 4, 5, 9, 3};
        int[] hwarr6 = {-4, 1, 2, 3, 9, 5, 6, 4};
        int[] hwarr7 = {-4, 1, 2, 3, 4, 5, 9, 6};
        int[] hwarr8 = {-4, 1, 2, 3, 5, 9, 4, 6};
        System.out.println("Question 2:");
        SelectionSort(hwarr5);
        SelectionSort(hwarr6);
        SelectionSort(hwarr7);
        SelectionSort(hwarr8);
        System.out.println("Question 3:");
        ArrayList<Integer> hwarr10 = new ArrayList<>();
        for (int i = 0; i <= 30000; i++){
            hwarr10.add(i);
        }
        //System.out.println(hwarr10.toString());
        System.out.println(ArrayListSearch(hwarr10, 15));
        System.out.println(ArrayListSearch(hwarr10, 30));
        System.out.println(ArrayListSearch(hwarr10, 300));
        System.out.println(ArrayListSearch(hwarr10, 3000));
        int[] hwarr9 = {-4, 1, 2, 3, 4, 5, 6, 9};
        System.out.println("Question 4:");
        System.out.println(BinarySearch(hwarr9, 1));
        System.out.println(BinarySearch(hwarr9, 2));
        System.out.println(BinarySearch(hwarr9, 3));
        System.out.println(BinarySearch(hwarr9, 4));

    }
}
