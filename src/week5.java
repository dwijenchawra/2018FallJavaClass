/*
 * Made by Dwijen on 10/27/2018
 */
import java.util.ArrayList;
public class week5 {
    public static void main(String[] args) {
        //MULTI-DIMENSIONAL ARRAYS
        int[][] arr = new int[15][15];

        for (int i = 0; i < 15; i++){
            for (int j = 0; j < 15; j++){
                arr[i][j] = (i+1) * (j+1);
            }
        }
        for (int[] a : arr) {
            for (int x : a){
                System.out.printf("%4d", x);
            }
            System.out.println("");
        }

        boolean[] b = new boolean[10];
        System.out.println(b[0]);
        /*
        print array like this
        0 1 2
        1 1 2
        2 2 2...
         */
/*
        for (int i = 0; i < 15; i++){
            for (int j = 0; j < 15; j++){
                arr[i][j] = Math.max(i, j);
            }
        }

        for (int[] a : arr) {
            for (int x : a){
                System.out.printf("%4d", x);
            }
            System.out.println("");
        }

        for (int i = 0; i < 15; i++){
            for (int j = 0; j < 15; j++){
                if (i >= j) {
                    arr[i][j] = i;
                }else{
                    arr[i][j] = j;
                }
            }
        }

        for (int[] a : arr) {
            for (int x : a){
                System.out.printf("%4d", x);
            }
            System.out.println("");
        }
*/
        for (int i = 0; i < 15; i++){
            for (int j = 0; j < 15; j++){
                arr[i][j] = i;
                arr[j][i] = i;
            }
        }

        for (int[] a : arr) {
            for (int x : a){
                System.out.printf("%4d", x);
            }
            System.out.println("");
        }

        ArrayList<ArrayList<Integer>> arr1 = new ArrayList<>();

        for (int i = 0; i < 20; i++){
            arr1.add(new ArrayList<Integer>());
        }
        arr1.get(0).add(40);

        System.out.println(arr1.toString());
        /*
        HOW TO ITERATE OVER 2D ARRAYLIST
        for (int i = 0; i < arr1.size(); i++){
            for (int j = 0; i < arr1.size(); j++){

            }
        }
        */

        //HOMEWORK

        int[][] hwarr = new int[3][3];
        int counter = 1;
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                hwarr[i][j] = counter;
                counter++;
            }
        }
        for (int[] a : hwarr) {
            for (int x : a){
                System.out.printf("%4d", x);
            }
            System.out.println("");
        }
        int sum = 0;
        System.out.println("Rows");
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                sum += hwarr[i][j];
            }
            System.out.println(sum);
            sum = 0;
        }
        System.out.println("Columns");
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                sum += hwarr[j][i];
            }
            System.out.println(sum);
            sum = 0;
        }
        System.out.println("Diagonals");
        for (int i = 0; i < 3; i++){
            for (int j = 0; j < 3; j++){
                if (j == i){
                    System.out.println(hwarr[i][j]);
                }
            }
        }


    }
}
