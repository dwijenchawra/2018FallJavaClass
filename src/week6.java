/*
 * Made by Dwijen on 11/3/2018
 */

public class week6 {

    //RECURSION

    static int S(int n){
        if (n==1){
            return 1;
        }
        return n + S(n-1);
    }

    static int func(int n){
        return n + 1;
    }

    static int factorial(int n){
        if (n==1){
            return 1;
        }
        return n * factorial(n-1);
    }

    static int addodd(int n){
        if (n==1){
            return 1;
        }
        if (n % 2 == 0){
            return n + addodd(n-1);
        }else{
            return n + addodd(n-2);
        }
    }

    static int fibonacci(int n){
        if (n==1){
            return 0;
        }else if (n==2){
            return 1;
        }else {
            return fibonacci(n - 1) + fibonacci(n - 2);
        }
    }

    static int numchar(String x){
        if (x.isEmpty()){
            return 0;
        }
        return 1 + numchar(x.substring(1));
    }

    static int maxarray(int[] x, int n){
        if (n == x.length-1){
            return x[n];
        }
        return Math.max(x[n],maxarray(x, n + 1));
    }

    //HW

    static int digits(int x){
        return numchar(Integer.toString(x));
    }

    static boolean palindrome(String s){
        if (s.isEmpty()){
            return false;
        }
        else if (reverse(s).equals(s)){
                return true;
        }else{
            return false;
        }

    }
    static String reverse(String x){
        String reverse = "";
        char[] str = x.toCharArray();
        for (int i = x.length(); i > 0; i--){
            reverse += str[i-1];
        }
        System.out.println(reverse);
        return reverse;
    }
    public static void main(String[] args) {
        System.out.println(func(6));
        System.out.println(S(5));
        System.out.println(factorial(4));
        System.out.println(addodd(21));
        System.out.println(fibonacci(7));
        System.out.println(numchar("qwerty"));
        int[] x = {1, 6, 2, 3, 7, 12, 5, 2, 6, 3};
        System.out.println(maxarray(x, 0));

        //HW
        System.out.println("HW:");
        System.out.println(digits(45667745));
        System.out.println(palindrome("palindromeemordnilap"));


    }
}
